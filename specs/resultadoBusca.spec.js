const resultados = require('../pageObjects/resultadosDaBusca.po');

// Marca
describe('Busca por marca na tela de resultados', () => {
    var marca = 'honda'
    it('Dado que estou na tela de resultados', () => {
        resultados.open('/carros/estoque?tipoveiculo=carros&estadocidade=estoque');
    });
    it(`Quando inserir a marca ${marca} no campo de busca`, () => {
        resultados.realizaBusca(marca);
    });
    it('E clicar na marca exibida na barra de sugestão', () => {
        resultados.resultadoBusca.click();
    });
    it('Então deve filtrar os veículos exibindo de acordo com a marca selecionada', () => {
        expect(resultados.itemFiltrado[1].getText()).toContain(marca.toUpperCase());
    });
});

describe('Filtro de marcas na tela de resultados, selecionando entre as 9 marcas principais', () => {
    var marca = 'honda'
    it('Dado que estou na tela de resultados', () => {
        resultados.open('/carros/estoque?tipoveiculo=carros&estadocidade=estoque');
    });
    it('Quando selecionar uma marca entre as 9 principais', () => {
        resultados.filtraPrincipais(marca);
    });
    it('Então deve filtrar os veículos exibindo de acordo com a marca selecionada', () => {
        expect(resultados.itemFiltrado[1].getText()).toContain(marca.toUpperCase());
    });
});

describe('Filtro de marcas na tela de resultados, selecionando entre todas as marcas', () => {
    var marca = 'honda';
    it('Dado que estou na tela de resultados', () => {
        resultados.open('/carros/estoque?tipoveiculo=carros&estadocidade=estoque');
    });
    it('Quando selecionar a opção Ver todas as marcas', () => {
        resultados.abreTodasMarcas();
    });
    it(`E selecionar a marca ${marca}`, () => {
        resultados.buscaFiltro(marca, 2);
    });
    it('Então deve filtrar os veículos exibindo de acordo com a marca selecionada', () => {
        expect(resultados.itemFiltrado[1].getText()).toContain(marca.toUpperCase());
    });
});


// Modelo
describe('Busca por modelo na tela de resultados', () => {
    var modelo = 'city'
    it('Dado que estou na tela de resultados', () => {
        resultados.open('/carros/estoque?tipoveiculo=carros&estadocidade=estoque');
    });
    it(`Quando inserir o modelo ${modelo} no campo de busca`, () => {
        resultados.realizaBusca(modelo);
    });
    it('E clicar na marca e modelo exibidos na barra de sugestão', () => {
        resultados.resultadoBusca.click();
    });
    it('Então deve filtrar os veículos exibindo de acordo com o modelo selecionado', () => {
        expect(resultados.itemFiltrado[1].getText()).toContain(modelo.toUpperCase());
    });
});

describe('Filtro por modelo na tela de resultados, selecionando entre as 9 marcas principais', () => {
    var marca = 'honda';
    var modelo = 'city';
    it('Dado que estou na tela de resultados', () => {
        resultados.open('/carros/estoque?tipoveiculo=carros&estadocidade=estoque');
    });
    it(`Quando selecionar a marca ${marca} entre as 9 principais`, () => {
        resultados.filtraPrincipais(marca);
    });
    it(`E selecionar o modelo ${modelo}`, () => {
        resultados.novoFiltro(modelo, 1);
    });
    it('Então deve filtrar os veículos exibindo de acordo com a marca e modelo selecionado', () => {
        expect(resultados.itemFiltrado[1].getText()).toContain(marca.toUpperCase());
        expect(resultados.itemFiltrado[1].getText()).toContain(modelo.toUpperCase());
    });
});

describe('Filtro por modelo na tela de resultados, selecionando entre todas as marcas', () => {
    var marca = 'honda';
    var modelo = 'city';
    it('Dado que estou na tela de resultados', () => {
        resultados.open('/carros/estoque?tipoveiculo=carros&estadocidade=estoque');
    });
    it('Quando selecionar a opção Ver todas as marcas', () => {
        resultados.abreTodasMarcas();
    });
    it(`E selecionar a marca ${marca}`, () => {
        resultados.buscaFiltro(marca, 2);
    });
    it(`E selecionar o modelo ${modelo}`, () => {
        resultados.novoFiltro(modelo, 1);
    });
    it('Então deve filtrar os veículos exibindo de acordo com a marca e o modelo selecionado', () => {
        expect(resultados.itemFiltrado[1].getText()).toContain(marca.toUpperCase());
        expect(resultados.itemFiltrado[1].getText()).toContain(modelo.toUpperCase());
    });
});

// Versão
describe('Filtro por modelo na tela de resultados, selecionando entre as 9 marcas principais', () => {
    var marca = 'honda';
    var modelo = 'city';
    var versao = '1.5 sport';
    it('Dado que estou na tela de resultados', () => {
        resultados.open('/carros/estoque?tipoveiculo=carros&estadocidade=estoque');
    });
    it(`Quando selecionar a marca ${marca} entre as 9 principais`, () => {
        resultados.filtraPrincipais(marca);
    });
    it(`E selecionar o modelo ${modelo}`, () => {
        resultados.novoFiltro(modelo, 1);
    });
    it(`E selecionar a versão ${versao}`, () => {
        resultados.novoFiltro(versao, 0);
    });
    it('Então deve filtrar os veículos exibindo de acordo com a marca, modelo e versão selecionada', () => {
        expect(resultados.itemFiltrado[1].getText()).toContain(marca.toUpperCase());
        expect(resultados.itemFiltrado[1].getText()).toContain(modelo.toUpperCase());
        expect(resultados.itemFiltrado[1].getText()).toContain(versao.toUpperCase());
    });
});

describe('Filtro por modelo na tela de resultados, selecionando entre todas as marcas', () => {
    var marca = 'honda';
    var modelo = 'city';
    var versao = '1.5 sport';
    it('Dado que estou na tela de resultados', () => {
        resultados.open('/carros/estoque?tipoveiculo=carros&estadocidade=estoque');
    });
    it('Quando selecionar a opção Ver todas as marcas', () => {
        resultados.abreTodasMarcas();
    });
    it(`E selecionar a marca ${marca}`, () => {
        resultados.buscaFiltro(marca, 2);
    });
    it(`E selecionar o modelo ${modelo}`, () => {
        resultados.novoFiltro(modelo, 1);
    });
    it(`E selecionar a versão ${versao}`, () => {
        resultados.novoFiltro(versao, 0);
    });
    it('Então deve filtrar os veículos exibindo de acordo com a marca, modelo e versão selecionada', () => {
        expect(resultados.itemFiltrado[1].getText()).toContain(marca.toUpperCase());
        expect(resultados.itemFiltrado[1].getText()).toContain(modelo.toUpperCase());
        expect(resultados.itemFiltrado[1].getText()).toContain(versao.toUpperCase());
    });
});