const resultados = require('../pageObjects/resultadosDaBusca.po');

// Marca
describe('Filtro de marcas na tela de resultados, selecionando entre as 9 marcas principais', () => {
    var marca = 'honda'
    it('Dado que estou na tela de resultados', () => {
        resultados.open('/carros/estoque/?IdRevendedor=3834764&TipoVeiculo=carro s&anunciante=concession%C3%A1ria%7Cloja');
    });
    it('Quando selecionar uma marca entre as 9 principais', () => {
        resultados.filtraPrincipais(marca);
    });
    it('Então deve filtrar os veículos exibindo de acordo com a marca selecionada', () => {
        expect(resultados.itemFiltrado[2].getText()).toContain(marca.toUpperCase());
    });
});

describe('Filtro de marcas na tela de resultados, selecionando entre todas as marcas', () => {
    var marca = 'honda';
    it('Dado que estou na tela de resultados', () => {
        resultados.open('/carros/estoque/?IdRevendedor=3834764&TipoVeiculo=carro s&anunciante=concession%C3%A1ria%7Cloja');
    });
    it('Quando selecionar a opção Ver todas as marcas', () => {
        resultados.abreTodasMarcas();
    });
    it(`E selecionar a marca ${marca}`, () => {
        resultados.buscaFiltro(marca, 2);
    });
    it('Então deve filtrar os veículos exibindo de acordo com a marca selecionada', () => {
        expect(resultados.itemFiltrado[2].getText()).toContain(marca.toUpperCase());
    });
});


// Modelo
describe('Filtro por modelo na tela de resultados, selecionando entre as 9 marcas principais', () => {
    var marca = 'honda';
    var modelo = 'city';
    it('Dado que estou na tela de resultados', () => {
        resultados.open('/carros/estoque/?IdRevendedor=3834764&TipoVeiculo=carro s&anunciante=concession%C3%A1ria%7Cloja');
    });
    it(`Quando selecionar a marca ${marca} entre as 9 principais`, () => {
        resultados.filtraPrincipais(marca);
    });
    it(`E selecionar o modelo ${modelo}`, () => {
        resultados.novoFiltro(modelo, 1);
    });
    it('Então deve filtrar os veículos exibindo de acordo com a marca e modelo selecionado', () => {
        expect(resultados.itemFiltrado[2].getText()).toContain(marca.toUpperCase());
        expect(resultados.itemFiltrado[2].getText()).toContain(modelo.toUpperCase());
    });
});

describe('Filtro por modelo na tela de resultados, selecionando entre todas as marcas', () => {
    var marca = 'honda';
    var modelo = 'city';
    it('Dado que estou na tela de resultados', () => {
        resultados.open('/carros/estoque/?IdRevendedor=3834764&TipoVeiculo=carro s&anunciante=concession%C3%A1ria%7Cloja');
    });
    it('Quando selecionar a opção Ver todas as marcas', () => {
        resultados.abreTodasMarcas();
    });
    it(`E selecionar a marca ${marca}`, () => {
        resultados.buscaFiltro(marca, 2);
    });
    it(`E selecionar o modelo ${modelo}`, () => {
        resultados.novoFiltro(modelo, 1);
    });
    it('Então deve filtrar os veículos exibindo de acordo com a marca e o modelo selecionado', () => {
        expect(resultados.itemFiltrado[2].getText()).toContain(marca.toUpperCase());
        expect(resultados.itemFiltrado[2].getText()).toContain(modelo.toUpperCase());
    });
});

// Versão
describe('Filtro por modelo na tela de resultados, selecionando entre as 9 marcas principais', () => {
    var marca = 'honda';
    var modelo = 'city';
    var versao = '1.5 exl';
    it('Dado que estou na tela de resultados', () => {
        resultados.open('/carros/estoque/?IdRevendedor=3834764&TipoVeiculo=carro s&anunciante=concession%C3%A1ria%7Cloja');
    });
    it(`Quando selecionar a marca ${marca} entre as 9 principais`, () => {
        resultados.filtraPrincipais(marca);
    });
    it(`E selecionar o modelo ${modelo}`, () => {
        resultados.novoFiltro(modelo, 1);
    });
    it(`E selecionar a versão ${versao}`, () => {
        resultados.novoFiltro(versao, 0);
    });
    it('Então deve filtrar os veículos exibindo de acordo com a marca, modelo e versão selecionada', () => {
        expect(resultados.itemFiltrado[2].getText()).toContain(marca.toUpperCase());
        expect(resultados.itemFiltrado[2].getText()).toContain(modelo.toUpperCase());
        expect(resultados.itemFiltrado[2].getText()).toContain(versao.toUpperCase());
    });
});

describe('Filtro por modelo na tela de resultados, selecionando entre todas as marcas', () => {
    var marca = 'honda';
    var modelo = 'city';
    var versao = '1.5 exl';
    it('Dado que estou na tela de resultados', () => {
        resultados.open('/carros/estoque/?IdRevendedor=3834764&TipoVeiculo=carro s&anunciante=concession%C3%A1ria%7Cloja');
    });
    it('Quando selecionar a opção Ver todas as marcas', () => {
        resultados.abreTodasMarcas();
    });
    it(`E selecionar a marca ${marca}`, () => {
        resultados.buscaFiltro(marca, 2);
    });
    it(`E selecionar o modelo ${modelo}`, () => {
        resultados.novoFiltro(modelo, 1);
    });
    it(`E selecionar a versão ${versao}`, () => {
        resultados.novoFiltro(versao, 0);
    });
    it('Então deve filtrar os veículos exibindo de acordo com a marca, modelo e versão selecionada', () => {
        expect(resultados.itemFiltrado[2].getText()).toContain(marca.toUpperCase());
        expect(resultados.itemFiltrado[2].getText()).toContain(modelo.toUpperCase());
        expect(resultados.itemFiltrado[2].getText()).toContain(versao.toUpperCase());
    });
});