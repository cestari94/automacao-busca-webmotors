#  QA Automação - WebMotors Busca

Automatizar os cenários de front-end para regressão nos campos de busca de "Marca", "Modelo" e "Versão" para o estoque geral e para o estoque de um lojista.

**Este desafio foi feito em [WebdriverIO](https://webdriver.io)**

## Ferramentas

Foi usada a Framework WebDriver IO em NodeJS e Jasmine, executando o Selenium Standalone

### Pré requisitos

Baixar e instalar o NodeJS na máquina, o download pode ser realizado através do link: https://nodejs.org/en/download/

Após instalação do NodeJS, abrir o PowerShell ou outro terminal de preferência e executar o comando:
```
npm install -g yarn
```
Este irá instalar o Yarn como uma dependencia global na máquina.

### Instalação

clone o repositório na pasta de projetos e acesse o diretório pelo terminal

Dentro do diretório webmotors execute o comando abaixo para instalar o projeto:
```
yarn install
```

### Executando os testes

Após a instalação execute o comando abaixo para executar o script de teste:
```
yarn test
```

* **ATENÇÂO** Caso a tela apresente o captcha durante a execução, o teste automaticamente irá parar, o captcha deve ser feito manualmente e após conclusão deve digitar no terminal o comando abaixo para que os testes possam seguir.

```
.exit
```