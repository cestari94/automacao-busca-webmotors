exports.config = {
    baseUrl: 'https://www.webmotors.com.br',
    runner: 'local',
    specs: [
        './specs/*.spec.js'
    ],
    maxInstances: 1,
    capabilities: [{
        maxInstances: 1,
        browserName: 'chrome',
            'goog:chromeOptions': {
                args: ['start-maximized', '--log-level=3']
            }
    }],
    logLevel: 'silent',
    bail: 0,
    waitforTimeout: 10000,
    connectionRetryTimeout: 90000,
    connectionRetryCount: 3,
    services: ['selenium-standalone'],
    framework: 'jasmine',
    reporters: ['spec'],
    jasmineNodeOpts: {
        defaultTimeoutInterval: 6000 * 25 * 100 * 99,
        expectationResultHandler: function(passed, assertion) {
        }
    },
}
