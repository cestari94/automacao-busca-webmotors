class Resultados {
    get campoBusca() { return $('input#searchBar'); }
    get resultadoBusca() {return $('div.search-bar-result-name'); }
    get principaisMarcas() { return $$('span.CardMake'); }
    get linkTodasMarcas() { return $('div.Filters__line__see-more'); }
    get campoBuscaFiltro() { return $$('div.Filters__container__group--no-border > div > input.Form__field__noerror'); }
    get linkMaisFiltros() { return $('div.Filters__line--gray'); }
    get resultadosLista() { return $$('div.Filters__line__result'); }
    get itemFiltrado() { return $$('a.FilterResult__container__list__item'); }
    get verificacaoRobo() { return $('div.page-title-wrapper');}

    open(pagina) {
        browser.url(pagina);
        if(this.verificacaoRobo.isDisplayed()){
            browser.debug();
        }
        this.campoBusca.waitForDisplayed(5000);
    }

    abreTodasMarcas() {
        this.linkTodasMarcas.scrollIntoView();
        this.linkTodasMarcas.click();
    }

    realizaBusca(texto) {
        this.campoBusca.clearValue(texto);
        this.campoBusca.addValue(texto);
    }

    filtraPrincipais(marca) {
        var i = -1;
        var marcaAtual = '';
        while(marca != marcaAtual) {
            i++;
            marcaAtual = this.principaisMarcas[i].getAttribute('title').toLowerCase();
        }
        this.principaisMarcas[i].click();
    }

    buscaFiltro(texto, index) {
        if(texto) {
            this.campoBuscaFiltro[index].addValue(texto);
        }
        this.resultadosLista[0].click();
    }

    novoFiltro(texto, index) {
        this.linkMaisFiltros.click();
        this.buscaFiltro(texto, index);
    }
};

module.exports = new Resultados();